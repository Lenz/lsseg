#!/usr/local/bin/python
# coding: utf8

# lsseg -- Unsupervised Segmentation Using Letter Successor Counts
# Copyright 2013-2014 Lenz Furrer <lenz.furrer@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


'''
Command-line interface to the lsseg module.
'''


from __future__ import division, unicode_literals, print_function

import argparse
import sys
import logging
from pprint import PrettyPrinter

from lsseg import CharTrie, Segmenter, text_to_tries


CHAR_WINDOW = 5
PEAK_THRESHOLD = 10
PEAK_TYPE = 'freedom'


def main():
    '''
    Segment text from STDIN to STDOUT.
    '''
    ap = argparse.ArgumentParser(
        description='Perform unsupervised text segmentation.', add_help=False,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    segpar = ap.add_argument_group('segmentation parameters')
    segpar.add_argument(
        '-w', '--window', type=int, default=CHAR_WINDOW, metavar='N',
        help='character trie window (ie. trie depth)')
    segpar.add_argument(
        '-l', '--lowercase', action='store_true',
        help='use lowercased text in character tries')
    segpar.add_argument(
        '-p', '--peak-type', choices=['freedom', 'entropy'], default=PEAK_TYPE,
        help='type of peak calculation: degree of freedom (LSV) '
        'or entropy (LSE)')
    segpar.add_argument(
        '-t', '--peak-threshold', type=float, default=PEAK_THRESHOLD,
        metavar='N',
        help='threshold for inserting a segment boundary at a peak')

    fmt = ap.add_argument_group('formatting options')
    fmt.add_argument(
        '-s', '--separator', type=string_literal, default=r'\x20', metavar='C',
        help='use this string to separate segments in the output')
    fmt.add_argument(
        '-r', '--replacement', type=string_literal, default=r'\xA0', metavar='C',
        help='when the separator string (option -s) occurs in the input text, '
        'replace it with this string')
    fmt.add_argument(
        '-i', '--input-encoding', default='UTF8', metavar='ENC',
        help='character encoding used in the input stream')
    fmt.add_argument(
        '-o', '--output-encoding', default='UTF8', metavar='ENC',
        help='character encoding to use in the output stream')

    opt = ap.add_argument_group('processing options')
    opt.add_argument(
        '-c', '--load-chartries', nargs=2, metavar=('FW', 'BW'),
        help='load the chartries from dump files (two paths needed)')
    opt.add_argument(
        '-d', '--dump-chartries', nargs=2, metavar=('FW', 'BW'),
        help='save the chartries to dump files (two paths needed)')
    opt.add_argument(
        '-h', '--help', action='help',
        help='show this help message and exit')
    opt.add_argument(
        '-n', '--no-segmentation', action='store_true',
        dest='skip_segmentation',
        help='skip the segmentation step '
        '(this is useful when only saving a chartrie dump)')
    opt.add_argument(
        '-q', '--quiet', action='store_true',
        help='suppress info messages')

    args = ap.parse_args()

    loglevel = logging.WARN if args.quiet else logging.INFO
    logging.basicConfig(
        format='[%(levelname)s] %(asctime)s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=loglevel)
    del args.quiet

    segment_(infile=sys.stdin, outfile=sys.stdout, **vars(args))


def string_literal(s):
    '''
    Opt parsing: interpret s as if seen inside a Python unicode literal.
    '''
    return eval('u"%s"' % s.replace('"', r'\"'))


def segment_(**kwargs):
    '''
    Wrapper for decent printing of the settings.
    '''
    logging.info(
        'Segmentation started with the following settings: \n%s',
        PrettyPrinter().pformat(kwargs))
    segment(**kwargs)


def segment(infile=sys.stdin, input_encoding='UTF8',
            outfile=sys.stdout, output_encoding='UTF8',
            load_chartries=None, dump_chartries=None, skip_segmentation=False,
            window=CHAR_WINDOW, lowercase=False,
            peak_type=PEAK_TYPE, peak_threshold=PEAK_THRESHOLD,
            separator=' ', replacement='\xA0'):
    '''
    Segment text from infile to outfile.
    '''
    # Build or load the character tries.
    fw, bw = CharTrie(), CharTrie()
    if load_chartries is None:
        # Build forward and backward trie from the input text.
        logging.info('Building character tries...')
        if not skip_segmentation:
            # infile needs to be read twice -> store in memory
            infile = infile.readlines()
        for line in infile:
            line = line.decode(input_encoding).rstrip()
            fw, bw = text_to_tries(line, window, fw, bw,
                                   lowercase=lowercase)
    else:
        # Load the trie dump files.
        logging.info('Loading character tries...')
        for trie, fn in zip((fw, bw), load_chartries):
            with open(fn) as f:
                loaded_window = trie.loaddump(f)
                if loaded_window != window:
                    logging.warn('Character window changed to %d',
                                 loaded_window)
                    window = loaded_window

    # Save a dump of the character tries if desired.
    if dump_chartries is not None:
        logging.info('Writing character tries to dump files...')
        for trie, fn in zip((fw, bw), dump_chartries):
            with open(fn, 'w') as f:
                for d in trie.dumpdata():
                    f.write(d)

    # Perform the actual segmentation.
    if not skip_segmentation:
        logging.info('Segmenting input text...')
        segmodel = Segmenter(fw, bw, window=window, lowercase=lowercase,
                             peak=peak_type)
        for line in infile:
            line = line.decode(input_encoding).rstrip()
            segments = segmodel.segment(line, peak_threshold)
            outfile.write(
                separator.join(s.replace(separator, replacement)
                               for s in segments).encode(output_encoding))
            outfile.write(b'\n')

    logging.info('Finished.')


if __name__ == '__main__':
    main()
