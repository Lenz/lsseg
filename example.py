#!/usr/local/bin/python
# coding: utf8

# lsseg -- Unsupervised Segmentation Using Letter Successor Counts
# Copyright 2013-2014 Lenz Furrer <lenz.furrer@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


'''
Example usage of the lsseg module.
'''


from __future__ import division, unicode_literals, print_function

import sys
import codecs

from lsseg import CharTrie, Segmenter, text_to_tries


sys.stdin = codecs.getreader('UTF8')(sys.stdin)
sys.stdout = codecs.getwriter('UTF8')(sys.stdout)


CHAR_WINDOW = 5
LOWER_CASE = False
PEAK_THRESHOLD = 10
PEAK_TYPE = 'freedom'


def main():
    '''
    Segment text from STDIN and write to STDOUT in verticalised format.
    '''
    # Learn the character transitions.
    forward, backward = CharTrie(), CharTrie()
    input_copy = []
    for line in sys.stdin:
        line = line.rstrip()
        # Store the information in character tries.
        forward, backward = text_to_tries(line, CHAR_WINDOW, forward, backward,
                                          lowercase=LOWER_CASE)
        input_copy.append(line)

    # If you want to reuse the character tries, save them to disk using
    # CharTrie.dumpdata().  Read them back into memory with CharTrie.loaddump().

    # Segment the input text.
    segmodel = Segmenter(forward, backward, window=CHAR_WINDOW,
                         lowercase=LOWER_CASE, peak=PEAK_TYPE)
    for line in input_copy:
        # Verticalise the text (one segment per line).
        for segment in segmodel.segment(line, PEAK_THRESHOLD):
            print(segment)
        print()  # add a blank line to separate input lines.


if __name__ == '__main__':
    main()
