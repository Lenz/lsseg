#!/usr/bin/python
# coding: utf8

from setuptools import setup, find_packages

with open('README') as f:
    readme = f.read()

setup(name='lsseg',
      version='1.0',
      author='Lenz Furrer',
      author_email='lenz.furrer@uzh.ch',
      license='GPLv3',
      description='Python Package for Unsupervised Segmentation Using '
                  'Letter Successor Counts',
      long_description=readme,
      packages=find_packages())
